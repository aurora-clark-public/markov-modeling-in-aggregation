import pickle
import numpy as np

'generate count matrix from the clustering analysis to build a markov state model'

num_ext = 60
#lag_time_list defines the timestep/frequency at which the trajectories are saved in MD 
#for using without sliding window option i.e with sampling lagtime option, please fill the lag_time_list and make sliding_window list [1] 
lag_time_list = [10] 
#sliding_window indicate the gap between the previous and updated list. for ex. if one of the combination is 10ps-30ps for lagtime 20ps
#then, sliding_window is 2.
sliding_window = [1,2,5,10,15,20,25,50]
Count_Mat = np.zeros((num_ext,num_ext))
count_mat_list = []
output_txt = []
for each in lag_time_list:
    for item in sliding_window:
        with open("pic_10fs_scl.txt", "rb") as fp:
            b = pickle.load(fp)
        for_loop_number = 0
        final_combination={}
        Mat = 'Count_Mat_DF_' + str(item)
        Mat = np.zeros((8,8))
        for each in b:
            list2 = [] 
            list_pentamer_mono_switch = [] 
            if for_loop_number !=0:
                output_txt.append('for loop no {}:'.format(for_loop_number))
                output_txt.append('"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""')
                prev = b[for_loop_number-item]
                list_i=each        
                len_new_set=0
                master_set = []
                master_set_break=[]
                for j in range(0,len(list_i)):
                    list_set=[]
                    list_j = list_i[j]
                    current_set={}
                    for x in range(0,len(list_j)):              
                        new_val=list_j[x]
                        condition = 0
                        for pj in range(0,len(prev)): 
                            list_pj = prev[pj]                            
                            for px in range(0,len(list_pj)): 
                                old_val=list_pj[px] 
                                if new_val == old_val:
                                    old_val_length = len(list_pj)
                                    new_val_length = len(list_j)
                                    combination = ''
                                    new_set=set(list_j).intersection(list_pj)
                                    found = 0
                                    if current_set != new_set:
                                        current_set = new_set
                                        set_exists=0
                                        #master_set_break blank
                                        if master_set_break !=[]:                                                    
                                            for m in master_set_break:
                                                if m[0]==new_set:
                                                    set_exists=1
                                        if(set_exists==0):
                                            new_list = []
                                            #adding set
                                            new_list.append(new_set)
                                            #length of the set
                                            new_list.append(len(new_set))
                                            #previous it belong
                                            new_list.append(list_pj)
                                            #cheking add or divided
                                            if set(list_pj)==set(list_j):
                                                #output_txt.append("yes: mol-id matches")
                                                combination=str(len(list_pj))+'-'+str(len(list_j))
                                                #output_txt.append('complete match of mol-id and oligomeric state and the formation is: {} '.format( new_val))
                                                #print('mol_num(1) = {} Remain in same:'.format( new_val))
                                                condition=1  
                                                #suggest formation/addition
                                            elif(old_val_length == len(new_set)):
                                                if new_val_length > len(new_set):
                                                    new_list.append('+')
                                                    combination=str(old_val_length)+'-'+str(new_val_length)
                                                    new_list.append(combination)
                                                #suggest deformation                                                                                                                                                                                                   
                                            elif(old_val_length > len(new_set)):
                                                new_list.append('-')
                                                combination=str(old_val_length)+'-'+str(len(new_set))
                                                new_list.append(combination)
                                        if combination != '':
                                            if combination in final_combination:
                                                final_combination[combination]=final_combination[combination]+1
                                            else:
                                                final_combination[combination]=1
                                            x= combination.split('-')
                                            a= int(x[0])-1
                                            c= int(x[1])-1
                                            #this condition is to selectively defile the matrix size. higher number may lead to sparse and nonreversible matrix
                                            #matrix size accoringly need to be defined at the top
                                            if a <= 7 and c <= 7:
                                                Mat[a][c] += 1
                                            #adding new list it belong
                                            new_list.append(list_j)
                                            #print(master_set_break) 
                                            master_set_break.append(new_list)                                                                                      
                                        else:
                                            print(".")
                                            #output_txt.append("*Previously belong to smaller oligomer and set is {}".format(new_set))
                                            #print("*Previously belong to Smaller oligomer",new_set)
                                            #output_txt.append("so molecule_id {} added to bigger oligomer of {}".format(new_val,len_new_set))
                                            #print('(so molecule {} added to bigger Bond of {})'.format(new_val,len_new_set))                                                                  
                                if condition == 1:
                                    break                                                                                    
                            if condition == 1:
                                break
                        if condition == 1:
                            break
                    if list_set != []:
                        master_set.append(list_set)
                #output_txt.append("''''''''''''''''''''''''''''''''''''''''''''''''''''''''")
                #output_txt.append('Final Data for this loop: {}'.format(master_set_break))
                #print('Final Data for this loop ',master_set_break)
                output_txt.append('Updating Combination list: {}'.format(final_combination))
                #output_txt.append("''''''''''''''''''''''''''''''''''''''''''''''''''''''''")
                #print('Updating Combination list: ',final_combination)
            for_loop_number += 1
        output_txt.append(Mat)
        count_mat_list.append(Mat)
        #print(Count_Mat_DF)
    print(count_mat_list)
with open('markov_matrix.txt','w') as f:
    f.write(str(output_txt))


#LOAD THE COUNT MATRIX AS DATAFRAME

print(count_mat_list[0])
import pandas as pd
dfs = []
for each in count_mat_list:
    dfs.append(pd.DataFrame(each))



####implied timescale calculation

import numpy as np
from msmtools.estimation import transition_matrix, log_likelihood
###count_matrix to transition_matrix
lag_time_list = [10,20,50,100,150,200,250,500]

Tr_M_Pye = []
evals_list = []
evecs_list = []
its = []
for each in range(len(count_mat_list)):
    Tr_M_Pye.append(transition_matrix(count_mat_list[each]))
#eigenval from transition matrix

for each in Tr_M_Pye:
    evals, evecs = np.linalg.eig(each.T)
    evec1 = evecs[:,np.isclose(evals, 1)]
    Sorted_evals = sorted(evals,reverse=True)
    evals_list.append(Sorted_evals)
    evecs_list.append(evec1)

#PRINT EIGEN VALUE
print(evals_list[0])
print(evals_list[1])
print(evals_list[2])
print(evals_list[3])
print(evals_list[4])
print(evals_list[5])

#PRINT IMPLIED TIME SCALE
for i in range(len(evals_list)):
    for j in lag_time_list:
        t2 = -(j/np.log(evals_list[i][1]))
        t3 = -(j/np.log(evals_list[i][2]))
        t4 = -(j/np.log(evals_list[i][3]))
        its.append('{}, {}, {}'.format(t2,t3,t4))
print(its[0])
print(its[1])
print(its[2])
print(its[3])
print(its[4])
print(its[5])
print(its[6])
print(its[7])


#ITS FROM PYEMMA
import pyemma
from msmtools.analysis import timescales

its1 = timescales(Tr_M_Pye[0], tau=10, k=None, ncv=None, reversible=False, mu=None)
its2 = timescales(Tr_M_Pye[1], tau=20, k=None, ncv=None, reversible=False, mu=None)
its3 = timescales(Tr_M_Pye[2], tau=50, k=None, ncv=None, reversible=False, mu=None)
its4 = timescales(Tr_M_Pye[3], tau=100, k=None, ncv=None, reversible=False, mu=None)
its5 = timescales(Tr_M_Pye[4], tau=150, k=None, ncv=None, reversible=False, mu=None)
its6 = timescales(Tr_M_Pye[5], tau=200, k=None, ncv=None, reversible=False, mu=None)
its7 = timescales(Tr_M_Pye[6], tau=250, k=None, ncv=None, reversible=False, mu=None)
its8 = timescales(Tr_M_Pye[7], tau=500, k=None, ncv=None, reversible=False, mu=None)
print(its1,its2,its3,its4,its5,its6,its7,its8)

#NON-REVERSIBLE TO REVERSIBLE
get_ipython().run_line_magic('pylab', 'inline')
import pyemma.plots as mplt
import pyemma.msm as msm
import numpy as np
M = msm.markov_model(Tr_M_Pye[2])
w_nonrev = M.stationary_distribution

# make reversible
C = np.dot(np.diag(w_nonrev), Tr_M_Pye[2])
#print(C)
Csym = C + C.T
P2 = Csym / np.sum(Csym,axis=1)[:,np.newaxis]
M2 = msm.markov_model(P2)
#print(M2)
w = M2.stationary_distribution
# plot
P2pos = np.array([[1,4],[2,4],[3,4],[4,4],
                  [1,3],[2,3],[3,3],[4,3]], dtype=float)
mplt.plot_markov_model(P2, pos=P2pos, state_sizes=w, arrow_label_format="%1.2f");

####CK Test
from numpy.linalg import matrix_power
CK1 = matrix_power(Tr_M_Pye[3], 5)
#CK1 = Tr_M_Pye[0]
CK2 = Tr_M_Pye[7]
print(CK1)
print(CK2)


#NETWORK REPRESENTATION OF MSM
get_ipython().run_line_magic('pylab', 'inline')
import pyemma.plots as mplt
import pyemma.msm as msm
import numpy as np

P_rev_list = []
M_rev_list = []
stationary_rev = []
for each in Tr_M_Pye:
    M = msm.markov_model(each)
    w_nonrev = M.stationary_distribution
    C = np.dot(np.diag(w_nonrev), each)
    Csym = C + C.T
    P_rev = Csym / np.sum(Csym,axis=1)[:,np.newaxis]
    M_rev = msm.markov_model(P_rev)
    w_rev = M_rev.stationary_distribution
    P_rev_list.append(P_rev)
    M_rev_list.append(M_rev)
    stationary_rev.append(w_rev)
    
P2pos = np.array([[1,4],[2,4],[3,4],[4,4],
                  [1,3],[2,3],[3,3],[4,3]], dtype=float)
mplt.plot_markov_model(P_rev_list[2], pos=P2pos, state_sizes=stationary_rev[2], arrow_label_format="%1.2f");


###using reversible transition matrix
import pyemma
from msmtools.analysis import timescales
its_list = []
its1 = timescales(P_rev_list[0], tau=10, k=None, ncv=None, reversible=True, mu=None)
its2 = timescales(P_rev_list[1], tau=20, k=None, ncv=None, reversible=True, mu=None)
its3 = timescales(P_rev_list[2], tau=50, k=None, ncv=None, reversible=True, mu=None)
its4 = timescales(P_rev_list[3], tau=100, k=None, ncv=None, reversible=True, mu=None)
its5 = timescales(P_rev_list[4], tau=150, k=None, ncv=None, reversible=True, mu=None)
its6 = timescales(P_rev_list[5], tau=200, k=None, ncv=None, reversible=True, mu=None)
its7 = timescales(P_rev_list[6], tau=250, k=None, ncv=None, reversible=True, mu=None)
its8 = timescales(P_rev_list[7], tau=500, k=None, ncv=None, reversible=True, mu=None)
#its_list.append("{}, {}, {}, {}, {}, {}, {}".format(its1,its2,its3,its4,its5,its6,its7))
#print(its1,its2,its3,its4,its5,its6,its7,its8)
its_list.append(its1)
its_list.append(its2)
its_list.append(its3)
its_list.append(its4)
its_list.append(its5)
its_list.append(its6)
its_list.append(its7)
its_list.append(its8)

print(its_list[1][1])
for y in list(its_list):
    print(y[1])
    #print(each)



