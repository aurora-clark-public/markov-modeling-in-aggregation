# Markov Modeling in Aggregation

These codes support the identification of different aggregate/cluster states in solutions and subsequent Markov modeling of their interconversions. 

Written by Dr. Biswajit Sadhu, post-doctoral fellow in the Aurora Clark lab. Email is: biswajit.sadhu@wsu.edu

