import os
import numpy as np
import matplotlib.pyplot as plt
import pylab as plb
import subprocess
from shutil import copyfile, copy2
import collections
import itertools
import pickle
from scipy.spatial.distance import squareform, pdist
import pandas as pd
import gudhi as gd
from math import sqrt
from sklearn.kernel_approximation import RBFSampler
from sklearn.preprocessing import MinMaxScaler
from persim import sliced_wasserstein
from persim import persistent_entropy
from gudhi.representations import DiagramSelector, Clamping, Landscape, Silhouette, BettiCurve, ComplexPolynomial,\
  TopologicalVector, DiagramScaler, BirthPersistenceTransform,\
  PersistenceImage, PersistenceWeightedGaussianKernel, Entropy, \
  PersistenceScaleSpaceKernel, SlicedWassersteinDistance,\
  SlicedWassersteinKernel, BottleneckDistance, PersistenceFisherKernel
import networkx as nx
'''
Average betti number calculations  
prints persistence image, diagram, barcode and landscape for time= 1000, 25000, 50000, 100000
provide number of atoms in a single molecule(num_atom),  
'''

# traj parameters
traj_length = 100000
sampling_dt = 100


#directory for files
if os.path.exists('tda') == False:
	os.mkdir('tda')
os.chdir('tda')

# get molecule number and name
num_ext = 0
num_wat = 0
num_ntc = 0
with open('../../dt.top') as top:
	for line in top:
		if len(line) > 1:
			if line.split()[0] == 'DGA':
				ext = 'DGA'
				o_atom_num = '1'
				num_ext = int(line.split()[1]) + 1
			if line.split()[0] == 'DHO':
				ext2 = 'DHO'
				num_ext2 = int(line.split()[1])
			if line.split()[0] == 'SOL':
				num_wat = int(line.split()[1])
			if line.split()[0] == 'NTR':
                                num_ntc = int(line.split()[1])


#read the id number of first extractant
with open('../../nvt.gro') as gro:
	lines = gro.readlines()[1:]
	for line in lines:
		if 'DGA' in line:
			id_ext = int(line.split('DGA')[0])
			break
'''
###preparing xyz based on size##
with open("../../Cluster_analysis/100ps_cut12_lifetime/clust-outputs/pic_markov_1.txt", "rb") as fp:
	 b = pickle.load(fp)
counter = 0
L = []
for each in b:
	time = counter*100
	print('time:',time)
	num = 0
	for j in range(len(each)):
		list1 = []
		if j > 0:
			prev = len(each[j-1])
			for k in each[j]:
				k += id_ext
				list1.append(k)
			length = len(list1)
			idnum = str(list1)[1:-1].replace( ','  , ' ' )
			if length > 1:
				
				if length == prev:
					num += 1
					L.append(length)
				else:
					num = 0
				
				make_ndx = subprocess.Popen(('gmx_mpi_d','make_ndx','-f','../../nvt.tpr','-o','rog_%s_%s_%s.ndx' % (length,time,num)),
                                               stdin=subprocess.PIPE,stdout=subprocess.PIPE)
				stdin = make_ndx.communicate(input=('r %s & a O1 O2 O3 N1 N2 C8 C12 C16 C20 C24 C28 C32 C36 \nq\n' % idnum).encode())[0]
				# make .gro traj for ext and water
				make_gro = subprocess.Popen(('gmx_mpi_d','trjconv','-f','../../nvt-nopbc-100ns.xtc','-s','../../nvt.tpr','-b','%s' % time, '-e','%s' % time,'-n','rog_%s_%s_%s.ndx' % (length,time,num),'-o','DGA_%s_%s_%s.gro' % (length,time,num)),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
				stdin = make_gro.communicate(input=('r %s & a O1 O2 O3 N1 N2 C8 C12 C16 C20 C24 C28 C32 C36 \nq\n' % idnum).encode())[0]
				# convert .gro files to .xyz
				with open('DGA_%s_%s_%s.gro' % (length,time,num)) as gro:
					with open('DGA_%s_%s_%s.xyz' % (length,time,num),'w') as xyz:
						print('making xyz for time:',time)
						for i, line in enumerate(gro):
							if i ==0:
								pass
							elif i == 1:
								num_atoms = int(line.split()[0])
								xyz.write(str(num_atoms) + '\n')
								xyz.write('\n')
							elif (i > 1) & (i < num_atoms + 2):
								xyz.write(line.split()[1][0]+' ')
								xyz.write(str(round(1000*float(line.split()[-3]))/100)+' ')
								xyz.write(str(round(1000*float(line.split()[-2]))/100)+' ')
								xyz.write(str(round(1000*float(line.split()[-1]))/100)+'\n')
	counter += 1
	length_bin = np.bincount(L)
	print(length_bin)

'''
###########gudhi calculations on persistent diagram, barcode and average persistent betti number ###########################
number_same_cluster = 30
num_ext = 60
num_atom = 13
'''
if os.path.exists('picpbn') == False:
	os.mkdir('picpbn')
if os.path.exists('dimpic') == False:
	os.mkdir('dimpic')
'''
if os.path.exists('coeffone2one') == False:
	os.mkdir('coeffone2one')
def zerolistmaker(n):
	listofzeros = [0] * n
	return listofzeros

for length in np.arange(4,11,1):
	for_loop_number = 0
	sum_pbe_frame = []
	picpbn = []
	wrong_xyz_list = []
	DIAG = []
	oneDIAG = []
	SWD = []
	counter = 0 
	for time in range(0,traj_length,sampling_dt):
		print('time:',time)
		for num in range(0,number_same_cluster,1):
			if os.path.exists('DGA_%s_%s_%s.xyz' % (length,time,num)) == True:
				#name = 'DGA_%s_%s_%s.xyz' % (length,time,num)
				counter += 1
				f = open('DGA_%s_%s_%s.xyz' % (length,time,num), "r")
				g = open("corrected-coord.txt", "w")
				for line in f:
					if line.strip():
						g.write("\t".join(line.split()[1:]) + "\n")
				f.close()
				fl_coords = []
				with open("corrected-coord.txt", "r") as g:
					lines = g.readlines()
					desired_lines = lines[1::1]
					for line in desired_lines:
						coords = [np.array(line.replace("'", "").rstrip('\n').replace('\t', ' ').split(' '))]
						for arr in coords:
							l = arr.astype(float)
							fl_coords.append(l)

				arr = np.asarray(fl_coords)
				#split array based on cluster size variable length
				split = np.array_split(arr,length)
				split_for_av_coeff = np.array_split(arr[0::13],length)
				#dimension of matrix for ph
				dim = num_atom * length
				mat = np.zeros((dim,dim))
				dummy = np.zeros((dim,dim))
				#dimension of matrix for average clustering coefficient
				dimen = length
				mat_clust = np.zeros((dimen,dimen))
				#######
				ndx = 0
				mdx = 0
				for i in range(len(split)):
					for odx,k in enumerate(split[i]):
						ndx += 1
						mdx = 0
						for j in range(len(split)):
							for jdx, l in enumerate(split[j]):
								mdx += 1
								
								#one to one interaction and intramolecular atoms are at infinite position
								if i == j:
									mat[ndx-1][mdx-1] = 100
									mat[mdx-1][ndx-1] = 100
								if i != j and odx == jdx:
									dist = sqrt(([k][0][0] - [l][0][0])**2 + ([k][0][1] - [l][0][1])**2 + ([k][0][2] - [l][0][2])**2)
									mat[ndx-1][mdx-1] = dist
									mat[mdx-1][ndx-1] = dist
								if i != j and odx != jdx:
									mat[ndx-1][mdx-1] = 100
									mat[mdx-1][ndx-1] = 100
				print(mat)
            			#################################################################				
				ndx = 0
				mdx = 0
				for i in range(len(split_for_av_coeff)):
					for odx,k in enumerate(split_for_av_coeff[i]):
						ndx += 1
						mdx = 0
						for j in range(len(split_for_av_coeff)):
							for jdx, l in enumerate(split_for_av_coeff[j]):
								mdx += 1
								if i == j:
									mat_clust[ndx-1][mdx-1] = 0
									mat_clust[mdx-1][ndx-1] = 0
								if i != j:
									dist = sqrt(([k][0][0] - [l][0][0])**2 + ([k][0][1] - [l][0][1])**2 + ([k][0][2] - [l][0][2])**2)
									if dist < 12:
										mat_clust[ndx-1][mdx-1] = 1
										mat_clust[mdx-1][ndx-1] = 1
				
				print(mat_clust)
				#matrix_to_barcode
				mat_dist0 = mat
				#sf1 = squareform(pdist(arr, 'euclidean'))
				#dist_list = [pd.DataFrame(sf1)]
				#mat_dist0 = dist_list[0]
				skeleton_0 = gd.RipsComplex(distance_matrix=mat_dist0, max_edge_length=50.0)
				Rips_simplex_tree_0 = skeleton_0.create_simplex_tree(max_dimension=3)
				Rips_simplex_tree_0.dimension()
				result_str = 'Rips complex is of dimension ' + repr(Rips_simplex_tree_0.dimension()) + ' - ' +     repr(Rips_simplex_tree_0.num_simplices()) + ' simplices - ' +     repr(Rips_simplex_tree_0.num_vertices()) + ' vertices.'

				print(result_str)
				'''
				if Rips_simplex_tree_0.dimension() == 0 or np.array_equal(mat,dummy) == True or np.any(mat[:, 0] > 50) == True:
					if os.path.exists('picpbn/pic_wrong_xyz_%s.txt' %str(length)):
						with open('picpbn/pic_wrong_xyz_%s.txt' %str(length),'rb') as rfp:
							wrong_xyz_list = pickle.load(rfp)
					wrong_xyz_list.append(name)
					with open('picpbn/pic_wrong_xyz_%s.txt' %str(length),'wb') as wxyz:
						pickle.dump(wrong_xyz_list, wxyz)
				'''
				#if Rips_simplex_tree_0.dimension() > 0 and np.array_equal(mat,dummy) == False and np.any(mat[:, 0] > 50) == False:
				if Rips_simplex_tree_0.dimension() > 0 and np.array_equal(mat,dummy) == False and np.any(np.logical_and(mat > 50, mat < 100)) == False:
					#fmt = '%s -> %.2f'
					#for filtered_value in Rips_simplex_tree_0.get_filtration():
					#print(fmt % tuple(filtered_value))
					
					####MAIN ELEMENT FOR CALCULATION####################
					'''
					if os.path.exists('dimpic/diag_pic_%s.txt' %str(length)):
						with open('dimpic/diag_pic_%s.txt' %str(length),'rb') as rfp:
							DIAG = pickle.load(rfp)
					'''
					
					if os.path.exists('coeffone2one/detail_pic_%s.txt' %str(length)):
						with open('coeffone2one/detail_pic_%s.txt' %str(length),'rb') as rfp:
							detail = pickle.load(rfp)
					
					if os.path.exists('coeffone2one/121diag_pic_%s.txt' %str(length)):
						with open('coeffone2one/121diag_pic_%s.txt' %str(length),'rb') as rfp:
							oneDIAG = pickle.load(rfp)
					
					diag = Rips_simplex_tree_0.persistence(min_persistence=0)
					#DIAG.append(diag)
					#oneDIAG.append(diag)
					detail = zerolistmaker(13)
					count_hole1 = 0
					sum_hole1 = 0
					v0 = zerolistmaker(1)
					name = 'DGA_%s_%s_%s.xyz' % (length,time,num)
					detail[0] = name
					for i , v in diag:
						#if i == 0:
							#D0i.append(v)
						if i == 1:
							count_hole1 += 1
							persist1 = v[1]-v[0]
							#detail[count_hole1] = persist1
							sum_hole1 += persist1
							v0.append(v[0])	
							#D1i.append(v)
							copy2('DGA_%s_%s_%s.xyz' % (length,time,num), 'coeffone2one/DGA_%s_%s_%s.xyz' % (length,time,num))
							oneDIAG.append('DGA_%s_%s_%s.xyz' % (length,time,num))
							oneDIAG.append(diag) 
					detail[1] = sum_hole1
					detail[2] = count_hole1
					if len(v0) > 1:
						detail[3] = sorted(v0)[1]
					# make .gro traj for ext and water
					#if os.path.exists('moi_%s_%s_%s.xvg' % (length,time,num)) == False:
					moi = subprocess.Popen(('gmx_mpi_d','gyrate','-f','../../nvt-nopbc-100ns.xtc','-s','../../nvt.tpr','-n','rog_%s_%s_%s.ndx' % (length,time,num),'-moi','-o','moi_%s_%s_%s.xvg' % (length,time,num),'-b','%s' % time,'-e','%s' % time),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
					stdin = moi.communicate(input=('8 \nq\n').encode())[0]
					if os.path.exists('rog_%s_%s_%s.xvg' % (length,time,num)) == False:
						rog = subprocess.Popen(('gmx_mpi_d','gyrate','-f','../../nvt-nopbc-100ns.xtc','-s','../../nvt.tpr','-n','rog_%s_%s_%s.ndx' % (length,time,num),'-o','rog_%s_%s_%s.xvg' % (length,time,num),'-b','%s' % time,'-e','%s' % time),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
						stdin = rog.communicate(input=('8 \nq\n').encode())[0]
						polystat = subprocess.Popen(('gmx_mpi_d','polystat','-f','../../nvt-nopbc-100ns.xtc','-s','../../nvt.tpr','-n','rog_%s_%s_%s.ndx' % (length,time,num),'-o','pst_%s_%s_%s.xvg' % (length,time,num),'-b','%s' % time,'-e','%s' % time),stdin=subprocess.PIPE,stdout=subprocess.PIPE)
						stdin = polystat.communicate(input=('8 \nq\n').encode())[0]
					shape_param = []
					np.set_printoptions(precision=3)
					with open('rog_%s_%s_%s.xvg' % (length,time,num),'r') as rog:
						lines = rog.readlines()
						rgval = str(lines[27]).replace('\n','').replace("''",'').replace("' ", " ")
						rgval = (np.array(rgval.strip("' ").split(   ))).astype(np.float)
						detail[4] = rgval[1]
						#rog.close()
					with open('pst_%s_%s_%s.xvg' % (length,time,num),'r') as pst:
						lines = pst.readlines()
						moment = str(lines[28]).replace('\n','').replace("''",'').replace("' ", " ")
						moment = (np.array(moment.strip("' ").split(   ))).astype(np.float)
						T1 = moment[3]
						T2 = moment[4]
						T3 = moment[5]
						bdef1 = T1 - (0.5*(T2+T3))
						cdef = T2-T3
						shapeaniso = ((3/2)*(((T1*T1)+(T2*T2)+(T3*T3))/((T1+T2+T3)*(T1+T2+T3))))-(1/2)
						detail[5] = bdef1
						detail[6] = cdef
						detail[7] = shapeaniso
						shape_param.extend((time,name,rgval,bdef1,cdef,shapeaniso))
						print(shape_param)	 
					
					with open('moi_%s_%s_%s.xvg' % (length,time,num),'r') as moi:
						lines = moi.readlines()
						inertia = str(lines[27]).replace('\n','').replace("''",'').replace("' ", " ")
						inertia = (np.array(inertia.strip("' ").split(   ))).astype(np.float)
						I1 = inertia[2]
						I2 = inertia[3]
						I3 = inertia[4]
						Imin = min(I1,I2,I3)
						Iav = (I1+I2+I3)/3
						eccentricity = 1-(Imin/Iav)
						detail[8] = eccentricity
						print('eccentricity:', eccentricity)
					#average clustering coeffecient
					nx_mat_clust = nx.from_numpy_matrix(mat_clust)
					conn_comp = nx.connected_component_subgraphs(nx_mat_clust)
					for c in conn_comp:
						av_clust_coeff = nx.average_clustering(c)
					print('av_clust_coeff:',av_clust_coeff)
					detail[9] = av_clust_coeff
					detail[10] = time
					detail[11] = length
					detail[12] = num
					with open("coeffone2one/shape_info_%s.txt" % length, 'a+') as fp:
						fp.seek(0)
						data = fp.read(100)
						if len(data) > 0:
							fp.write("\n")
						fp.write(str(shapeaniso))
							
					with open("coeffone2one/detail_info.txt", 'a+') as dp:
						dp.seek(0)
						data = dp.read(100)
						if len(data) > 0:
							dp.write("\n")
						dp.write(str(detail))
							
	
					# convert .gro files to .xyz
					##dimension wise array 
					#D0i = np.array(D0i)
					#D1i = np.array(D1i)

					'''
					with open('dimpic/diag_pic_%s.txt' %str(length),'wb') as wfp0:
						pickle.dump(DIAG, wfp0)
					'''
					
					with open('coeffone2one/detail_pic_%s.txt' %str(length),'wb') as wfp0:
						pickle.dump(detail, wfp0)
					
					with open('coeffone2one/121diag_pic_%s.txt' %str(length),'wb') as wfp1:
						pickle.dump(oneDIAG, wfp1)
					'''
					####persistent entropy calculation using GUDHI#######
					print('f_loop',for_loop_number)
					print('counter',counter)
					D0i = []
					D1i = []
					Ent = []
					prevD1i = []
					if os.path.exists('dimpic/entropy_pic_%s.txt' %str(length)):
						with open('dimpic/entropy_pic_%s.txt' %str(length),'rb') as rfp:
							Ent = pickle.load(rfp)

					if os.path.exists('diag1swt_pic_%s.txt' %str(counter-1)):
						with open('diag1swt_pic_%s.txt' %str(counter-1),'rb') as rfp:
							prevD1i = pickle.load(rfp)
					
					print('prevD1i',prevD1i)	
					if os.path.exists('dimpic/swd_pic_%s.txt' %str(length)):
						with open('dimpic/swd_pic_%s.txt' %str(length),'rb') as rfp:
							SWD = pickle.load(rfp)
					
					for i , v in diag:
						if i == 0:
							D0i.append(v)
						if i == 1:
							D1i.append(v) 
					##dimension wise array 
					D0i = np.array(D0i)
					D1i = np.array(D1i)
					#loading previous diag-dim1 values
					prevD1i = np.array(prevD1i)
					#print('prevD1i',prevD1i)
					#print('D1i', D1i)
					swd = sliced_wasserstein(D1i, prevD1i)
					print('swd:',swd)
					SWD.append(swd)
					#storing present diag-dim1 as previous for next loop
					prevD1i = np.array(D1i)	

					diags0i = [D0i]
					diags1i = [D1i]
					#diags0i = DiagramSelector(use=True, point_type="finite").fit_transform(diags0i)
					#diags0i = DiagramScaler(use=True, scalers=[([0,1], MinMaxScaler())]).fit_transform(diags0i)
					#diags0i = DiagramScaler(use=True, scalers=[([1], Clamping(maximum=.9))]).fit_transform(diags0i)
					
					#ET = Entropy(mode="scalar")
					#et0 = ET.fit_transform(diags0i)
					#Ent.append(str(et0[0,:]))

					#diags1i = DiagramSelector(use=True, point_type="finite").fit_transform(diags1i)
					#diags1i = DiagramScaler(use=True, scalers=[([0,1], MinMaxScaler())]).fit_transform(diags1i)
					#diags1i = DiagramScaler(use=True, scalers=[([1], Clamping(maximum=.9))]).fit_transform(diags1i)
					
					#et1 = ET.fit_transform(diags1i)
					#Ent.append(str(et1[0,:]))
					if length > 3:
						E1 = persistent_entropy.persistent_entropy(diags1i, keep_inf=False, val_inf=None, normalize=True)
						Ent.append(E1)
						print('E1:',E1)

						with open('dimpic/entropy_pic_%s.txt' %str(length),'wb') as wfp0:
							pickle.dump(Ent, wfp0)

					with open('diag1swt_pic_%s.txt' %str(counter),'wb') as wfp1:
						pickle.dump(prevD1i, wfp1)

					with open('dimpic/swd_pic_%s.txt' %str(length),'wb') as wfp2:
						pickle.dump(SWD, wfp2)

					
					###PIC CREATION ENDS####
					if os.path.exists('picpbn') == False:
						os.mkdir('picpbn')
					if time == 1000 or time == 75000 or time == 100000:
						gd.plot_persistence_barcode(diag,colormap = plt.cm.Dark2.colors,alpha=1)
						plt.savefig('picpbn/DGA_%s_%s_%s_pbarcode.png' % (length,time,num), dpi=600)
						plt.clf()
						gd.plot_persistence_diagram(diag,colormap = plt.cm.Dark2.colors,alpha=1,legend=True)
						plt.savefig('picpbn/DGA_%s_%s_%s_pdiagram.png' % (length,time,num), dpi=600) 
						plt.clf()
						#dimension wise segregation for persistent image generation
						D0i = []
						D1i = []
						for i , v in diag:
							if i == 0:
								D0i.append(v)
							if i == 1:
								D1i.append(v) 
						D0i = np.array(D0i)
						D1i = np.array(D1i)
						diags0i = [D0i]
						diags1i = [D1i]
						diags0i = DiagramSelector(use=True, point_type="finite").fit_transform(diags0i)
						diags0i = DiagramScaler(use=True, scalers=[([0,1], MinMaxScaler())]).fit_transform(diags0i)
						diags0i = DiagramScaler(use=True, scalers=[([1], Clamping(maximum=.9))]).fit_transform(diags0i)

						PI = PersistenceImage(bandwidth=.1, weight=lambda x: x[1], im_range=[0,1,0,1], resolution=[100,100])
						pi = PI.fit_transform(diags0i)
						plt.imshow(np.flip(np.reshape(pi[0], [100,100]), 0))
						plt.savefig('dimpic/DGA_%s_%s_%s_pimage_d0.png' % (length,time,num), dpi=600)
						plt.clf()

						diags1i = DiagramSelector(use=True, point_type="finite").fit_transform(diags1i)
						diags1i = DiagramScaler(use=True, scalers=[([0,1], MinMaxScaler())]).fit_transform(diags1i)
						diags1i = DiagramScaler(use=True, scalers=[([1], Clamping(maximum=.9))]).fit_transform(diags1i)
						
						pi = PI.fit_transform(diags1i)
						plt.imshow(np.flip(np.reshape(pi[0], [100,100]), 0))
						plt.savefig('dimpic/DGA_%s_%s_%s_pimage_d1.png' % (length,time,num), dpi=600)
						plt.clf()
						###printing landscape and image
						D = []
						for i , v in diag:
							D.append(v)
						D = np.array(D)
						diags = [D]
						diags = DiagramSelector(use=True, point_type="finite").fit_transform(diags)
						diags = DiagramScaler(use=True, scalers=[([0,1], MinMaxScaler())]).fit_transform(diags)
						diags = DiagramScaler(use=True, scalers=[([1], Clamping(maximum=.9))]).fit_transform(diags)
						LS = Landscape(resolution=1000)
						L = LS.fit_transform(diags)
						plt.plot(L[0][:1000])
						plt.plot(L[0][1000:2000])
						plt.plot(L[0][2000:3000])
						#plt.title("Landscape")
						#plt.show()
						plt.savefig('picpbn/DGA_%s_%s_%s_plandscape.png' % (length,time,num), dpi=600)
						plt.clf()

						PI = PersistenceImage(bandwidth=.1, weight=lambda x: x[1], im_range=[0,1,0,1], resolution=[100,100])
						pi = PI.fit_transform(diags)
						plt.imshow(np.flip(np.reshape(pi[0], [100,100]), 0))
						#plt.title("Persistence Image")
						#plt.show()
						plt.savefig('picpbn/DGA_%s_%s_%s_pimage.png' % (length,time,num), dpi=600)
						plt.clf()
					######################AVERAGE PERSISTENT BETTI NUMBER###################################
					print('f_loop',for_loop_number)
					###PIC CREATION START####				
					#if os.path.exists('picpbn') == False:
					#os.mkdir('picpbn')
					if os.path.exists('picpbn/pic_pbn_%s.txt' %str(length)):
						with open('picpbn/pic_pbn_%s.txt' %str(length),'rb') as rfp:
							picpbn = pickle.load(rfp)	
					#fixing window of plot(euclidean distance)
					i = np.arange(0,51,0.5)
					j = np.arange(0.5,51.5,0.5)
					dim_zip = len(list(zip(i,j)))
					#print(dim_zip)
					betti_number_list = []
					for i, j in zip(i,j):
						betti_number = Rips_simplex_tree_0.persistent_betti_numbers(i,j)
						betti_number_list.append(betti_number)
					picpbn.append(betti_number_list)
					with open('picpbn/pic_pbn_%s.txt' %str(length),'wb') as wfp0:
						pickle.dump(picpbn, wfp0)
					
					###PIC CREATION ENDS####
		for_loop_number += 1
	#	print('sum_pbe_frame:',sum_pbe_frame)
	#print('sum_pbe_frame',sum_pbe_frame)
	with open('picpbn/wrong_xyz.txt','w') as wx:
		wx.write(str(wrong_xyz_list))
	'''
#os.system('rm *.gro')
#os.system('rm *.ndx')
os.system('rm diag1swt_pic*')
'''
###############file processing for final output###############################################################
for length in np.arange(2,num_ext+1,1):
	if os.path.exists('picpbn/pic_pbn_%s.txt' % length) == True:
		with open('picpbn/pic_pbn_%s.txt' % length, "rb") as fp:
			b = pickle.load(fp)
		B0 = []
		B1 = []
		B2 = []
		for each in b:
			Y = np.dstack(each)
			shape = np.shape(Y)
			padded_array = np.zeros((1,3,dim_zip))
			padded_array[:shape[0],:shape[1]] = Y
			K = padded_array.reshape(3,1,dim_zip)
			b0 = (K[0])
			b1 = (K[1])
			b2 = (K[2])
			B0.append(b0)
			B1.append(b1)
			B2.append(b2)
		lengthb0 = len(B0)
		B_ALL_SUM = []
		B_ALL_AV = []
		B0_SUM = [sum(row[i] for row in B0) for i in range(len(B0[0]))]
		B1_SUM = [sum(row[i] for row in B1) for i in range(len(B1[0]))]
		B2_SUM = [sum(row[i] for row in B2) for i in range(len(B2[0]))]
		#print(B0_SUM)
		B_ALL_SUM.append(B0_SUM)
		B_ALL_SUM.append(B1_SUM)
		B_ALL_SUM.append(B2_SUM)

		B0_AV = np.divide(B0_SUM,lengthb0)
		B1_AV = np.divide(B1_SUM,lengthb0)
		B2_AV = np.divide(B2_SUM,lengthb0)
		#print(B0_AV)
		B_ALL_AV.append(B0_AV)
		B_ALL_AV.append(B1_AV)
		B_ALL_AV.append(B2_AV)
		with open('picpbn/pic_pbn_%s_sum.txt' %str(length),'wb') as wfp0:
			pickle.dump(B_ALL_SUM, wfp0)	
		with open('picpbn/pic_pbn_%s_av.txt' %str(length),'wb') as wfp0:
			pickle.dump(B_ALL_AV, wfp0)

#########calculation ends##################
'''
